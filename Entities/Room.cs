﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FurnitureAPI.Entities
{
    public class Room
    {
        public Room()
        {
            States = new List<RoomState>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime Created { get; set; }

        public List<RoomState> States { get; set; }

        public bool Deleted { get; set; }

    }
}