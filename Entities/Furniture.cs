﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FurnitureAPI.Entities
{
    public class Furniture
    {
        public int Id { get; set; }
        public FurnitureType Type { get; set; }
        public int TypeId { get; set; }
    }
}