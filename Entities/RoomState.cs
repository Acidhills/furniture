﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FurnitureAPI.Entities
{
    public class RoomState
    {
        public RoomState()
        {
            Furnituries = new List<Furniture>();
        }

        public int RoomId { get; set; }

        public DateTime Date { get; set; }

        public List<Furniture> Furnituries { get; set; }
        public bool Deleted { get; set; }
    }
}