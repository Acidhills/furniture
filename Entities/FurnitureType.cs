﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FurnitureAPI.Entities
{
    public class FurnitureType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}