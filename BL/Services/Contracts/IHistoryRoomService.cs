﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL.Entities;
using FurnitureAPI.BL.Entities;

namespace BL.Services.Contracts
{
    public interface IHistoryRoomService
    {
        List<HistoryRoomVM> GetRoomsHistory();
        List<ShortHistoryRoomVM> GetShortRoomsHistory();
    }
}
