﻿using FurnitureAPI.BL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FurnitureAPI.BL.Services.Contracts
{
    public interface IRoomService
    {
        RoomVM Create(string name,
            DateTime date);

        bool Delete(string foom,
                    string to,
                    DateTime date);

        RoomVM Get(string name, DateTime? date = null);

        List<RoomVM> GetRooms(DateTime? date = null);

    }
}
