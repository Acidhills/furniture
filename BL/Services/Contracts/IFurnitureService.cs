﻿using FurnitureAPI.BL.Entities;
using System;
using System.Collections.Generic;
using FurnitureAPI.Entities;

namespace FurnitureAPI.BL.Services.Contracts
{
    public interface IFurnitureService
    {
        FurnitureVM Create(int typeId,
                           string destination,
                           DateTime date);

        List<FurnitureVM> Move(int typeId,
                               string from,
                               string to,
                               DateTime date);

        bool Delete(int id);


        IEnumerable<FurnitureType> GetTypes();
    }
}