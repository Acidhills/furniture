﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL.Entities;
using BL.Services.Contracts;
using FurnitureAPI.BL.Entities;
using FurnitureAPI.BL.Services.Implementations;
using FurnitureAPI.DAL.Repositories.Contracts;
using FurnitureAPI.Entities;

namespace BL.Services.Implementations
{
    public class HistoryRoomService : ServiceBase,IHistoryRoomService
    {
        public HistoryRoomService(IRoomRepo roomRepo, IRoomStateRepo roomStateRepo) : base(roomRepo, roomStateRepo)
        {
        }
        public List<HistoryRoomVM> GetRoomsHistory()
        {
            return _roomRepo.GetRoomsByDate(null).Select(CreateHistoryRoomVm).ToList();
        }


        public List<ShortHistoryRoomVM> GetShortRoomsHistory()
        {

            return _roomRepo.GetRoomsByDate(null).Select(CreateShortHistoryRoomVm).ToList();
        }

        protected HistoryRoomVM CreateHistoryRoomVm(Room room)
        {
            return new HistoryRoomVM()
            {
                Created = room.Created.ToString(dateFormat),
                Name = room.Name,
                States = room.States.Select(x => CreateRoomVM(room, x.Date)).ToList()
            };
        }
        protected ShortHistoryRoomVM CreateShortHistoryRoomVm(Room room)
        {
            return new ShortHistoryRoomVM()
            {
                Created = room.Created.ToString(dateFormat),
                Name = room.Name,
                Dates = room.States.Select(x => x.Date.ToString(dateFormat)).ToList()
            };
        }

    }
}
