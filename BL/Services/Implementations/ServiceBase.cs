﻿using FurnitureAPI.BL.Entities;
using FurnitureAPI.DAL.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using FurnitureAPI.Entities;

namespace FurnitureAPI.BL.Services.Implementations
{
    public abstract class ServiceBase
    {
        protected const string dateFormat = "MM/dd/yyyy";
        protected readonly IRoomRepo _roomRepo;
        protected readonly IRoomStateRepo _roomStateRepo;
        public ServiceBase(IRoomRepo roomRepo,
                           IRoomStateRepo roomStateRepo)
        {
            _roomRepo = roomRepo;
            _roomStateRepo = roomStateRepo;
        }
        protected RoomVM CreateRoomVM(Room room, DateTime? date = null)
        {
            var states = room.States.Where(x => !x.Deleted);
            var state = date != null ? states.FirstOrDefault(x => x.Date == date) : states.OrderByDescending(x => x.Date).FirstOrDefault();
            return new RoomVM
            {
                Date = (date ?? state?.Date
                            ?? room.Created).ToString(dateFormat),
                Furnitures = state?.Furnituries.Select(CreateFurnitureVm).ToList() ?? new List<FurnitureVM>(),
                Name = room.Name
            };
        }

        protected FurnitureVM CreateFurnitureVm(Furniture furniture)
        {
            return new FurnitureVM { Id = furniture.Id, TypeName = furniture.Type.Name };
        }
    }
}