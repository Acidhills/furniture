﻿using FurnitureAPI.BL.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using FurnitureAPI.BL.Entities;
using FurnitureAPI.DAL.Repositories.Contracts;
using FurnitureAPI.Entities;

namespace FurnitureAPI.BL.Services.Implementations
{
    public class FurnitureService : ServiceBase, IFurnitureService
    {
        private IFurnitureRepo _furRepo;

        public FurnitureService(IRoomRepo roomRepo,
                                IRoomStateRepo roomStateRepo,
                                IFurnitureRepo furRepo):base(roomRepo,roomStateRepo)
        {
            _furRepo = furRepo;
        }
        public FurnitureVM Create(int typeId, string destination,DateTime date)
        {
            FurnitureVM result = null;
            var roomState = _roomStateRepo.Get(destination, date);
            if(roomState == null)
            {
                var room = _roomRepo.GetRoom(destination);
                if (room != null && room.Created >= date)
                {
                    roomState = room.States.FirstOrDefault(x => x.Date == date) ?? new RoomState { Date = date, RoomId = room.Id };
                }

            }
            if(roomState != null)
            {
                var fur = _furRepo.AddOrUpdate(new Furniture {TypeId = typeId, Type = new FurnitureType() {Id =  typeId, Name = ""} });

                if(fur != null)
                {
                    roomState.Furnituries.Add(fur);
                    result = _roomStateRepo.AddOrUpdate(roomState) != null ? CreateFurnitureVm(fur) : null;
                }
            }
            return result;
        }

        public List<FurnitureVM> Move(int typeId, string from, string to, DateTime date)
        {
            List<FurnitureVM> result = null;
            var rfrom = _roomRepo.GetRoom(from);
            var rto = _roomRepo.GetRoom(to);
            if (rfrom != null
               && rfrom.Created >= date
               && rto != null
               && rto.Created >= date)
            {
                var fromState = rfrom.States.FirstOrDefault(x => x.Date == date) ?? new RoomState();
                var toState = rto.States.FirstOrDefault(x => x.Date == date) ?? new RoomState() { Date = date, RoomId = rto.Id };
                var moving = fromState.Furnituries.Where(x => x.TypeId == typeId);
                toState.Furnituries.AddRange(moving);
                fromState.Furnituries.RemoveAll(x => moving.Any(z => z.Id == x.Id));
                result = _roomStateRepo.AddOrUpdate(fromState) != null && _roomStateRepo.AddOrUpdate(toState) != null 
                    ? moving.Select(x => CreateFurnitureVm(x)).ToList() : null;

            }
            return result;
        }

        public bool Delete(int id)
        {
            return _furRepo.Delete(id);
        }

        public IEnumerable<FurnitureType> GetTypes()
        {
            return _furRepo.GetTypes();
        }
    }
}