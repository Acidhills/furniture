﻿using FurnitureAPI.BL.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using FurnitureAPI.BL.Entities;
using FurnitureAPI.DAL.Repositories.Contracts;
using FurnitureAPI.Entities;

namespace FurnitureAPI.BL.Services.Implementations
{
    public class RoomService : ServiceBase, IRoomService
    {
        public RoomService(IRoomRepo roomRepo,
                           IRoomStateRepo roomStateRepo) : base(roomRepo, roomStateRepo)
        {
        }

        public RoomVM Create(string name, DateTime date)
        {
            var room = _roomRepo.GetRoom(name);
            if (room == null)
                room = new Room() {Name =  name, Created = DateTime.Now };
            var state = room.States.FirstOrDefault(x => x.Date.Date == date.Date);
            if (state != null)
                state.Deleted = true;
            else
                room.States.Add(new RoomState() { Date = date });
            
            room  = _roomRepo.AddOrUpdate(room);
            RoomVM result = null;
            if (room != null)
                result = CreateRoomVM(room);
            return result;
        }

        public bool Delete(string from, string to, DateTime date)
        {
            var result = false;
            var rfrom = _roomRepo.GetRoom(from);
            var rto = _roomRepo.GetRoom(to);
            if(rfrom != null 
               && rfrom.Created <= date
               && rto != null
               && rto.Created <= date)
            {
                var fromState = rfrom.States.FirstOrDefault(x => x.Date == date) ?? new RoomState();
                var toState = rto.States.FirstOrDefault(x => x.Date == date) ?? new RoomState() { Date = date, RoomId = rto.Id };
                toState.Furnituries.AddRange(fromState.Furnituries);
                fromState.Furnituries = null;
                fromState.Deleted = true;
                result = _roomStateRepo.AddOrUpdate(fromState) != null && _roomStateRepo.AddOrUpdate(toState) != null;

            }
            return result;
        }

        public RoomVM Get(string name, DateTime? date)
        {
            RoomVM result = null;
            var room = _roomRepo.GetRoom(name);
            if (room != null)
            {
                result = CreateRoomVM(room, date);
            }
            return result;
        }

        public List<RoomVM> GetRooms(DateTime? date = null)
        {
            return _roomRepo.GetRoomsByDate(date).Select(x => CreateRoomVM(x, date)).ToList();
        }
        


    }
}