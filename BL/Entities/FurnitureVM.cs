﻿namespace FurnitureAPI.BL.Entities
{
    public class FurnitureVM
    {
        public int Id { get; set; }
        public string TypeName { get; set; }
    }
}