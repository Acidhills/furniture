﻿using System.Collections.Generic;

namespace FurnitureAPI.BL.Entities
{
    public class HistoryRoomVM
    {
        public HistoryRoomVM()
        {
            States = new List<RoomVM>();
        }
        
        public string Name { get; set; }
        public string Created { get; set; }
        public List<RoomVM> States { get; set; }

    }
}