﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Entities
{
    public class ShortHistoryRoomVM
    {
        public string Name { get; set; }
        public string Created { get; set; }
        public List<string> Dates { get; set; }
    }
}
