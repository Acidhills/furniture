﻿using System.Collections.Generic;

namespace FurnitureAPI.BL.Entities
{
    public class RoomVM
    {
        public string Name { get; set; }
        public string Date { get; set; }
        public List<FurnitureVM> Furnitures { get; set; }
    }
}