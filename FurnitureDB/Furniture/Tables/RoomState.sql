﻿CREATE TABLE [dbo].[RoomState]
(
	[Id] INT NOT NULL PRIMARY KEY,
	[RoomId] INT NOT NULL,
	[Date] date NOT NULL,
	[Deleted] bit NOT NULL DEFAULT(0)
)
