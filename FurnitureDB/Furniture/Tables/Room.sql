﻿CREATE TABLE [dbo].[Room]
(
	[Id] INT NOT NULL PRIMARY KEY,
	[Name] varchar(250) NOT NULL ,
	[Created] date NOT NULL DEFAULT(GETDATE()),
	[Deleted] bit NOT NULL DEFAULT(0)
	CONSTRAINT UN_RoomName UNIQUE([Name]) 

)
