﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Http;
using System.Web.Optimization;
using FurnitureAPI.App_Start;
using Newtonsoft.Json;

namespace FurnitureAPI
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            AutofacStart.Start();
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            FurnitureAPI.Areas.GUI.BundleConfig.RegisterBundles(BundleTable.Bundles);
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.InstalledUICulture;
            //GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings =
            //        new JsonSerializerSettings
            //        {
            //            DateFormatHandling = DateFormatHandling.IsoDateFormat,
            //            DateTimeZoneHandling = DateTimeZoneHandling.Unspecified,
            //            Culture = CultureInfo.InvariantCulture
            //        };
        }
    }
}