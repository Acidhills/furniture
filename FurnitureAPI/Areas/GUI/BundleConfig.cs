﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace FurnitureAPI.Areas.GUI
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/gui/common").Include("~/areas/gui/Scripts/app.js")
                                                                .IncludeDirectory("~/areas/gui/Scripts/","*.js",true));

        }
    }
}