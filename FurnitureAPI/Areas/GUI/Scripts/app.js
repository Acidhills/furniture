﻿var furnitureApp = angular.module('furnitureApp', ['ui.router']);
furnitureApp.constant('locale.format', 'MM/DD/YYYY');
furnitureApp.config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
    $locationProvider.html5Mode(true);
    $urlRouterProvider.otherwise('/');
    $stateProvider
        .state('rooms', {
            url: '/',
            templateUrl: '/areas/GUI/static/Rooms.html'
        })
        .state('addRoom', {
            url: '/add',
            templateUrl: '/areas/GUI/static/AddRoom.html'
        })
        .state('deleteRoom', {
            url: '/delete-room?:room&:date',
            templateUrl: '/areas/GUI/static/DeleteRoom.html'
        })
        .state('roomDetails', {
            url: '/details/{name}?:date',
            templateUrl: '/areas/GUI/static/DetailsRoom.html'
        })
        .state('addFurniture', {
            url: '/add-fur?:room&:date',
            templateUrl: '/areas/GUI/static/AddFurniture.html'
        }).state('history', {
            url: '/history',
            templateUrl: '/areas/GUI/static/AddFurniture.html'
        });
});