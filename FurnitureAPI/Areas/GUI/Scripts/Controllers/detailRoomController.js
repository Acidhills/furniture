﻿furnitureApp.controller("detailRoomController", ['rooms', 'furniture', '$stateParams',
    function (roomsProvider, furnitureProvider, $stateParams) {
        var self = this;
        load();
        self.deleteFurniture = function(id) {
            furnitureProvider.del(id).success(function () {
                load();
            });
        }
        function load() {

            roomsProvider.getRoom($stateParams.name, $stateParams.date).success(function (result) {
                self.room = result;
            });
        }
    }
]);