﻿furnitureApp.controller("historyRoomController", ['historyRoom', 
    function (history) {
        var self = this;
        load();
        function load() {

            history.getHistory().success(function (result) {
                self.history = result;
            });
        }
    }
]);