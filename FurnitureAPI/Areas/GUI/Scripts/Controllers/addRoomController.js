﻿furnitureApp.controller("addRoomController", ['rooms', '$state', 'locale.format',
    function (roomsProvider, $state,locale) {
        var self = this;
        self.name = '';
        self.date = moment().format(locale);
        self.add = function () {
            roomsProvider.add({ date: self.date, name: self.name }).then(function () {
                $state.go('rooms');
            });
        }
        load();

        function load() {
            roomsProvider.getRooms().success(function (result) {
                self.rooms = result;
            });
            
        }
    }
]);