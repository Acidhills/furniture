﻿furnitureApp.controller("roomsController", ['rooms', '$state',
    function (roomsProvider, $state) {
        var self = this;
        self.rooms = [];
        self.date = null;
        self.filter = function() {
            roomsProvider.getRooms(self.date).success(function (result) {
            self.rooms = result;
        });
        }
        roomsProvider.getRooms().success(function(result) {
            self.rooms = result;
        });
    }
]);