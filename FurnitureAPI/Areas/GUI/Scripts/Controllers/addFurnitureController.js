﻿furnitureApp.controller("addFurnitureController", ['furniture','$state', '$stateParams',
    function (furnitureProvider, $state, $stateParams) {
        var self = this;
        self.room = $stateParams.room;
        self.date = $stateParams.date;
        self.type = null;
        load();
        
        self.add = function () {
            furnitureProvider.add({ date: self.date, room: self.room, type: self.type }).then(function () {
                $state.go('roomDetails',{name:self.room,date:self.date});
            });
        }

        function load() {
            furnitureProvider.getTypes().success(function (result) {
                self.types = result;
            });
            
        }
    }
]);