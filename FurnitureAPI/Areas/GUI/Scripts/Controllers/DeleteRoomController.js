﻿furnitureApp.controller("deleteRoomController", ['rooms', '$state', '$stateParams',
    function (roomsProvider, $state, $stateParams) {
        var self = this;
        self.room = $stateParams.room;
        self.date = $stateParams.date;
        self.destinationRoom = null;
        load();
        self.delete = function () {
            roomsProvider.del({ date: self.date, room: self.room, destination: self.destinationRoom }).then(function () {
                $state.go('rooms');
            });
        }

        function load() {
            roomsProvider.getRooms(self.date).success(function (result) {
                self.rooms = result.filter(function (val) { return val.Name != self.room; });
            });
            
        }
    }
]);