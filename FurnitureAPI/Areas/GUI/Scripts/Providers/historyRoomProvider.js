﻿furnitureApp.provider('historyRoom', [
    function () {
        var _url = null;
        this.setUrl = function (url) {
            _url = url;
        };
        this.$get = ['$http', function ($http) {
            _url = _url || '';
            return {
                getHistory: function (name, date) {
                    var getUrl = _url + '/API/Room/History/';
                    return $http.get(getUrl);
                },
                getShortHistory: function (date) {
                    var getUrl = _url + '/API/Room/History/short';
                    return $http.get(getUrl);
                }
            }
        }];
    }
])