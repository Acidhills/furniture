﻿furnitureApp.provider('furniture', [
    function () {
        var _url = null;
        this.setUrl = function (url) {
            _url = url;
        };
        this.$get = ['$http', 'locale.format', function ($http,locale) {
            _url = _url || '';
            return {
                add: function (options) {
                    return $http.post(_url + '/API/Furniture/' + options.type + '?date=' + moment(options.date, locale).format(locale) + '&room=' + encodeURI(options.room));
                },

                del: function (id) {
                    return $http.delete(_url + '/API/Furniture/' + id);
                },
                getTypes:function() {
                    return $http.get(_url + '/API/Furniture/types');
                }
            }
        }];
    }
])