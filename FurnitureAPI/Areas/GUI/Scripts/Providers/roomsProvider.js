﻿furnitureApp.provider('rooms', ['locale.format',
    function (locale) {
        var _url = null;
        this.setUrl = function (url) {
            _url = url;
        };
        this.$get = ['$http', function ($http) {
            _url = _url || '';
            return {
                getRoom: function(name, date) {
                    var getUrl = _url + '/API/Room/'+name;
                    if (date)
                        getUrl += '?date=' + moment(date, locale).format(locale);
                    return $http.get(getUrl);
                },
                getRooms: function (date) {
                    var getUrl = _url + '/API/Room/Query';
                    if (date)
                        getUrl += '?date=' + moment(date, locale).format(locale);
                    return $http.get(getUrl);
                },
                add: function(room) {
                    return $http.post(_url + '/API/Room/' + encodeURI(room.name) + '?date=' + moment(room.date, locale).format(locale));
                },
                del: function(options) {
                    return $http.delete(_url + '/API/Room/' + encodeURI(options.room)
                                                            + '?destination=' + encodeURI(options.destination)
                                                            + '&date=' + moment(options.date, locale).format(locale));
                }
            }
        }];
    }
])