﻿using System.Web.Mvc;

namespace FurnitureAPI.Areas.GUI
{
    public class GUIAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "GUI";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "GUI_default",
                "gui/{*url}",
                new {controller ="Room", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}