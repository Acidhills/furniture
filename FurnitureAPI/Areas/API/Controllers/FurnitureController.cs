﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using FurnitureAPI.BL.Services.Contracts;
using FurnitureAPI.Entities;

namespace FurnitureAPI.Areas.API.Controllers
{
    [RoutePrefix("API")]
    public class FurnitureController : BaseAPIController
    {
        private IFurnitureService _furnitureService;
        public FurnitureController(IFurnitureService furnitureService)
        {
            _furnitureService = furnitureService;
        }
        [HttpPost]
        [Route("Furniture/{typeId}")]
        public bool Create(int typeId, string room,DateTime date)
        {
            return _furnitureService.Create(typeId, room, date) != null;
        }

        [HttpPost]
        [Route("Furniture/{typeId}")]
        public bool Move(int typeId, string from, string to, DateTime date)
        {
            var result = _furnitureService.Move(typeId, from, to, date);
            return result != null;
        }

        [HttpDelete]
        [Route("Furniture/{id}")]
        public bool Delete(int id)
        {
            return _furnitureService.Delete(id);
        }

        [HttpGet]
        [Route("Furniture/types")]
        public List<FurnitureType> GetTypes()
        {
            return _furnitureService.GetTypes().ToList();
        }
    }
}
