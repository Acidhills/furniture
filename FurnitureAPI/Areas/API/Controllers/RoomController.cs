﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FurnitureAPI.BL.Entities;
using FurnitureAPI.BL.Services.Contracts;

namespace FurnitureAPI.Areas.API.Controllers
{
    [RoutePrefix("API")]
    public class RoomController : BaseAPIController
    {
        private IRoomService _roomService;
        private IFurnitureService _furnitureService;
        public RoomController(IRoomService roomService, IFurnitureService furnitureService)
        {
            _roomService = roomService;
            _furnitureService = furnitureService;
        }

        [HttpGet]
        [Route("Room/{name}")]
        public RoomVM Get(string name, DateTime? date = null)
        {
            return _roomService.Get(name, date);
        }

        [HttpPost]
        [Route("Room/{name}")]
        public bool Create(string name, DateTime date)
        {
            return _roomService.Create(name, date) != null; ;
        }
        [HttpDelete]
        [Route("Room/{room}")]
        public bool Delete(string room, string destination, DateTime date)
        {
            return _roomService.Delete(room, destination, date);
        }
        [HttpGet]
        [Route("Room/Query")]
        public List<RoomVM> Query(DateTime? date = null)
        {
            return _roomService.GetRooms(date);
        }
    }
}
