﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BL.Entities;
using BL.Services.Contracts;
using FurnitureAPI.BL.Entities;

namespace FurnitureAPI.Areas.API.Controllers
{

    [RoutePrefix("API")]
    public class HistoryRoomController : BaseAPIController
    {
        private IHistoryRoomService _hRoomService;

        public HistoryRoomController(IHistoryRoomService hRoomService)
        {
            _hRoomService = hRoomService;
        }

        [HttpGet]
        [Route("Room/History")]
        public List<HistoryRoomVM> History()
        {
            return _hRoomService.GetRoomsHistory();
        }
        [HttpGet]
        [Route("Room/History/short")]
        public List<ShortHistoryRoomVM> ShortHistory()
        {
            return _hRoomService.GetShortRoomsHistory();
        }
    }
}
