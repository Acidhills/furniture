﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FurnitureAPI.Controllers
{
    public class HomeController : Controller
    {
        //Самый простой способ задать дефолтный урл при страте))
        public ActionResult Index()
        {
            return Redirect("/gui");
        }
    }
}