﻿using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using BL.Services.Contracts;
using BL.Services.Implementations;
using FurnitureAPI.BL.Services.Contracts;
using FurnitureAPI.BL.Services.Implementations;
using FurnitureAPI.DAL.Repositories.Contracts;
using FurnitureAPI.DAL.Repositories.Implementations;

namespace FurnitureAPI.App_Start
{
    public class AutofacStart
    {
        public static void Start()
        {

            var builder = new ContainerBuilder();
            builder.RegisterType<FurnitureRepo>().As<IFurnitureRepo>();
            builder.RegisterType<RoomRepo>().As<IRoomRepo>();
            builder.RegisterType<RoomStateRepo>().As<IRoomStateRepo>();

            builder.RegisterType<FurnitureService>().As<IFurnitureService>();
            builder.RegisterType<RoomService>().As<IRoomService>();
            builder.RegisterType<HistoryRoomService>().As<IHistoryRoomService>();

            builder.RegisterControllers(Assembly.GetExecutingAssembly());
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            var config = GlobalConfiguration.Configuration;
            builder.RegisterWebApiFilterProvider(config);

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

        }
    }
}