﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace FurnitureAPI.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jQuery").Include("~/Scripts/jquery-1.*"));
   
               bundles.Add(new ScriptBundle("~/bundles/common").Include("~/Scripts/angular.js")
                                                               .Include("~/Scripts/angular-ui-router.js")
                                                               .Include("~/Scripts/date.format.js"));
            
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include("~/Scripts/bootstrap*").Include("~/Scripts/moment-with-locales.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/bootstrap*"));
        }
    }


}