﻿using System;
using System.Collections.Generic;
using System.Linq;
using FurnitureAPI.DAL.Repositories.Contracts;
using FurnitureAPI.Entities;

namespace FurnitureAPI.DAL.Repositories.Implementations
{
    public class RoomRepo :IRoomRepo
    {
        public static List<Room> rooms;

        static RoomRepo()
        {
            var firstroomId = new Random().Next();
            var secroomId = firstroomId + 1;
            rooms = new List<Room>()
        {
            new Room()
            {
                Created = DateTime.Today,
                Id = firstroomId,
                Name = "ololo",
                States = new List<RoomState>()
                {
                    new RoomState()
                    {
                        RoomId = firstroomId,
                        Date = DateTime.Today.AddDays(1),
                        Furnituries = new List<Furniture>()
                        {
                            new Furniture()
                            {
                                Id = new Random().Next(),
                                TypeId = 1,
                                Type = new FurnitureType() {Id = 1,Name = "Стул"}
                            },
                            new Furniture()
                            {
                                Id = new Random().Next(),
                                TypeId = 2,
                                Type = new FurnitureType() {Id = 2,Name = "Стол"}
                            }
                        }
                    }
                }
            },
            new Room()
            {
                Created = DateTime.Today,
                Id = secroomId,
                Name = "ululu",
                States = new List<RoomState>()
                {
                    new RoomState()
                    {
                        RoomId =secroomId,
                        Date = DateTime.Today.AddDays(1),
                        Furnituries = new List<Furniture>()
                        {
                            new Furniture()
                            {
                                Id = new Random().Next(),
                                TypeId = 1,
                                Type = new FurnitureType() {Id = 1,Name = "Стул"}
                            },
                            new Furniture()
                            {
                                Id = new Random().Next(),
                                TypeId = 2,
                                Type = new FurnitureType() {Id = 2,Name = "Стол"}
                            }
                        }
                    }
                }
            }
        };
        }
        public Room AddOrUpdate(Room room)
        {
            var myroom = rooms.FirstOrDefault(x => x.Name == room.Name);
            if (myroom != null)
            {
                myroom.Created = room.Created;
                myroom.Deleted = room.Deleted;
                myroom.Name = myroom.Name;
                myroom.States = myroom.States;
            }
            else
            {
                room.Id = new Random().Next();
                rooms.Add(room);
            }
            return myroom;
        }

        public Room GetRoom(string name)
        {
            return rooms.FirstOrDefault(x => x.Name == name);
        }

        public List<Room> GetRoomsByDate(DateTime? date = null)
        {
            var result = rooms.Where(x => !x.Deleted);
            if (date.HasValue)
            {
                result = result.Where(x => x.States.Exists(z => z.Date.Date == date.Value.Date));
            }
            return result.ToList();
        }
    }
}