﻿using System;
using System.Linq;
using FurnitureAPI.DAL.Repositories.Contracts;
using FurnitureAPI.Entities;

namespace FurnitureAPI.DAL.Repositories.Implementations
{
    public class RoomStateRepo: IRoomStateRepo
    {
        public RoomState AddOrUpdate(RoomState state)
        {
            RoomState result = null;
            var room = RoomRepo.rooms.FirstOrDefault(x => x.Id == state.RoomId);
            if (room != null)
            {
                var temp = room.States.FirstOrDefault(x => x.Date.Date == state.Date.Date);
                if (temp != null)
                {
                    temp.Deleted = state.Deleted;
                    temp.Furnituries = state.Furnituries;
                    result = temp;
                }
                else
                {
                    room.States.Add(state);
                    result = state;
                }
            }
            return result;
        }

        public RoomState Get(string room, DateTime? date = null)
        {
            RoomState result = null;
            var troom = RoomRepo.rooms.FirstOrDefault(x => x.Name == room);
            if (troom != null)
            {
                if (date.HasValue)
                {
                    result = troom.States.FirstOrDefault(x => x.Date.Date == date.Value.Date);
                }
                else
                {
                    result = troom.States.OrderByDescending(x => x.Date).FirstOrDefault();
                }

            }
            return result;
        }
    }
}