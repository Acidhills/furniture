﻿using System;
using System.Collections.Generic;
using System.Linq;
using FurnitureAPI.DAL.Repositories.Contracts;
using FurnitureAPI.Entities;

namespace FurnitureAPI.DAL.Repositories.Implementations
{
    public class FurnitureRepo : IFurnitureRepo
    {
        public Furniture AddOrUpdate(Furniture furniture)
        {
            if (furniture.Id == 0)
                furniture.Id = new Random().Next();
            return furniture;
        }

        public bool Delete(int id)
        {
            var result = false;
            var room = RoomRepo.rooms.FirstOrDefault(x => x.States.Any(z => z.Furnituries.Any(y => y.Id == id)));
            if (room != null)
            {
                var state = room.States.First(x => x.Furnituries.Any(z => z.Id == id));
                state.Furnituries.Remove(state.Furnituries.First(x => x.Id == id));
                result = true;
            }
            return result;
        }

        public IEnumerable<FurnitureType> GetTypes()
        {
            return new List<FurnitureType>()
            {
                new FurnitureType() {Id = 1, Name = "Стул"},
                new FurnitureType() {Id = 2, Name = "Стол"}

            };
        }
    }
}