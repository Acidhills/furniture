﻿using System;
using System.Collections.Generic;
using FurnitureAPI.Entities;

namespace FurnitureAPI.DAL.Repositories.Contracts
{
    public interface IRoomRepo
    {
        Room AddOrUpdate(Room room);

        Room GetRoom(string name);
        
        List<Room> GetRoomsByDate(DateTime? date = null);
    }
}
