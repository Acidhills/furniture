﻿using System;
using FurnitureAPI.Entities;

namespace FurnitureAPI.DAL.Repositories.Contracts
{
    public interface IRoomStateRepo
    {
        RoomState AddOrUpdate(RoomState state);

        RoomState Get(string room, DateTime? date = null);        
    }
}
