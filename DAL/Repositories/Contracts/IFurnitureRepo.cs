﻿using System.Collections.Generic;
using FurnitureAPI.Entities;

namespace FurnitureAPI.DAL.Repositories.Contracts
{
    public interface IFurnitureRepo
    {
        Furniture AddOrUpdate(Furniture furniture);

        bool Delete(int id);

        IEnumerable<FurnitureType> GetTypes();
    }
}
